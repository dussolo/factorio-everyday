# factorio-everyday

This repo contains mapshots of my Factorio plays. Just for fun. 
Located at: [https://dussolo.gitlab.io/factorio-everyday](https://dussolo.gitlab.io/factorio-everyday).

> Thanks goes to **[palats](https://mods.factorio.com/user/palats)** for his [Mapshot](https://mods.factorio.com/mod/mapshot) mod.
